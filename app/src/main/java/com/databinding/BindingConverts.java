package com.databinding;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.support.design.widget.TextInputEditText;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatRadioButton;
import android.widget.RadioGroup;

import com.databinding.utils.ObservableBoolean;
import com.databinding.utils.ObservableString;

public class BindingConverts {

    @BindingConversion
    public static String convertToString(ObservableString observableString) {
        return observableString.getValue();
    }

    @BindingConversion
    public static boolean convertToBoolean(ObservableBoolean observableBoolean) {
        return observableBoolean.getValue();
    }


    @BindingAdapter({"bindEditText"})
    public static void bindEditText(TextInputEditText view, final ObservableString string) {
        Pair<ObservableString, TextWatcherAdapter> pair = (Pair<ObservableString, TextWatcherAdapter>) view.getTag(R.id.dataBinding);
        if (pair == null || pair.first != string) {
            if (pair != null)
                view.removeTextChangedListener(pair.second);

            TextWatcherAdapter watcherAdapter = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    string.setValue(s.toString());
                }
            };

            view.setTag(R.id.dataBinding, new Pair<>(string, watcherAdapter));
            view.addTextChangedListener(watcherAdapter);
        }

        String newValue = string.getValue();
        if (!view.getText().toString().equals(newValue))
            view.setText(newValue);
    }


    @BindingAdapter({"bindRadioGroup"})
    public static void bindRadioGroup(RadioGroup view, final ObservableBoolean bindableBoolean) {
        if (view.getTag(R.id.dataBinding) != bindableBoolean) {
            view.setTag(R.id.dataBinding, bindableBoolean);
            view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    bindableBoolean.setValue(checkedId == group.getChildAt(1).getId());
                }
            });
        }
        boolean newValue = bindableBoolean.getValue();
        ((AppCompatRadioButton) view.getChildAt(newValue ? 1 : 0)).setChecked(true);
    }


}
