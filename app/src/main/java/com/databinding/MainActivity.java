package com.databinding;

import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;

import com.databinding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    public static final String LOGIN_INFO = "loginInfo ";

    private LoginInfo loginInfo;
    private ActivityMainBinding binding;
    private TextWatcherAdapter watcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UI();
        if (savedInstanceState == null)
            loginInfo = new LoginInfo();
        else {
            loginInfo = savedInstanceState.getParcelable(LOGIN_INFO);
            binding.setLoginInfo(loginInfo);
        }

        binding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginInfo.setLoginExecuted(true);
                if (loginInfo.validate(getResources()))
                    Snackbar.make(binding.getRoot(), loginInfo.getEmail().getValue() + " - " + loginInfo.getPassword().getValue(), Snackbar.LENGTH_LONG).show();
            }
        });
    }


    private void UI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        watcher = new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                loginInfo.validate(getResources());
            }
        };

        binding.email.addTextChangedListener(watcher);
        binding.password.addTextChangedListener(watcher);

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LOGIN_INFO, loginInfo);
    }

}
