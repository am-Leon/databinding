package com.databinding.utils;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

public class ObservableBoolean extends BaseObservable implements Parcelable {

    private boolean value;

    public ObservableBoolean() {
    }

    public ObservableBoolean(boolean value) {
        this.value = value;
    }

    protected ObservableBoolean(Parcel in) {
        value = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (value ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObservableBoolean> CREATOR = new Creator<ObservableBoolean>() {
        @Override
        public ObservableBoolean createFromParcel(Parcel in) {
            return new ObservableBoolean(in);
        }

        @Override
        public ObservableBoolean[] newArray(int size) {
            return new ObservableBoolean[size];
        }
    };

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        if (this.value != value) {
            this.value = value;
            notifyChange();
        }
    }
}
