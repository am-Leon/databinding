package com.databinding;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Patterns;

import com.databinding.utils.ObservableBoolean;
import com.databinding.utils.ObservableString;

public class LoginInfo implements Parcelable {

    private ObservableString email = new ObservableString();
    private ObservableString password = new ObservableString();
    private ObservableString emailError = new ObservableString();
    private ObservableString passwordError = new ObservableString();

    private ObservableBoolean existingUser = new ObservableBoolean();
    private boolean loginExecuted;

    public LoginInfo() {
    }

    private LoginInfo(Parcel in) {
        email = in.readParcelable(ObservableString.class.getClassLoader());
        password = in.readParcelable(ObservableString.class.getClassLoader());
        emailError = in.readParcelable(ObservableString.class.getClassLoader());
        passwordError = in.readParcelable(ObservableString.class.getClassLoader());
        existingUser = in.readParcelable(ObservableBoolean.class.getClassLoader());
        loginExecuted = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(email, flags);
        dest.writeParcelable(password, flags);
        dest.writeParcelable(emailError, flags);
        dest.writeParcelable(passwordError, flags);
        dest.writeParcelable(existingUser, flags);
        dest.writeByte((byte) (loginExecuted ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginInfo> CREATOR = new Creator<LoginInfo>() {
        @Override
        public LoginInfo createFromParcel(Parcel in) {
            return new LoginInfo(in);
        }

        @Override
        public LoginInfo[] newArray(int size) {
            return new LoginInfo[size];
        }
    };

    public ObservableString getEmail() {
        return email;
    }

    public void setEmail(ObservableString email) {
        this.email = email;
    }

    public ObservableString getPassword() {
        return password;
    }

    public void setPassword(ObservableString password) {
        this.password = password;
    }

    public ObservableString getEmailError() {
        return emailError;
    }

    public void setEmailError(ObservableString emailError) {
        this.emailError = emailError;
    }

    public ObservableString getPasswordError() {
        return passwordError;
    }

    public void setPasswordError(ObservableString passwordError) {
        this.passwordError = passwordError;
    }

    public ObservableBoolean getExistingUser() {
        return existingUser;
    }

    public void setExistingUser(ObservableBoolean existingUser) {
        this.existingUser = existingUser;
    }

    public boolean isLoginExecuted() {
        return loginExecuted;
    }

    public void setLoginExecuted(boolean loginExecuted) {
        this.loginExecuted = loginExecuted;
    }

    public void reset() {
        setEmail(null);
        setPassword(null);
        setEmailError(null);
        setPasswordError(null);
        setLoginExecuted(false);
    }


    public boolean validate(Resources res) {
        if (!isLoginExecuted())
            return true;

        int emailErrorRes = 0, passwordErrorRes = 0;

        if (getEmail().getValue().isEmpty())
            emailErrorRes = R.string.mandatory_field;

        else if (!Patterns.EMAIL_ADDRESS.matcher(getEmail().getValue()).matches())
            emailErrorRes = R.string.invalid_email;

        if (getExistingUser().getValue() && getPassword().getValue().isEmpty())
            passwordErrorRes = R.string.mandatory_field;

        emailError.setValue(emailErrorRes != 0 ? res.getString(emailErrorRes) : null);
        passwordError.setValue(passwordErrorRes != 0 ? res.getString(passwordErrorRes) : null);

        return emailErrorRes == 0 && passwordErrorRes == 0;
    }


}
